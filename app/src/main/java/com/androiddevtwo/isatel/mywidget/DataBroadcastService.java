package com.androiddevtwo.isatel.mywidget;

import android.app.ProgressDialog;
import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;

import com.androiddevtwo.isatel.mywidget.rss.Rss;
import com.androiddevtwo.isatel.mywidget.rss.RssItem;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.SimpleXmlConverterFactory;

/**
 * Created by AndroidDev2 on 11/11/2017.
 */

public class DataBroadcastService extends Service {

    private static final String TAG = "DataBroadcastService";
    public static final String BROADCAST_ACTION = "com.androiddevtwo.isatel.mywidget";
    private final Handler handler = new Handler();
    Intent intent;
    int counter = 0;
    private String API_BASE_URL = "http://www.cbi.ir/";
    ArrayList<RssItem> dataList;
    List coll = new ArrayList();
    String title;
    String description, link, author, category, publishDate;


    @Override
    public void onCreate() {
        super.onCreate();

        //this will b passed to startService && stopService
        intent = new Intent(BROADCAST_ACTION);
    }

    @Override
    public void onStart(Intent intent, int startId) {
        handler.removeCallbacks(sendUpdatesToUI);
        handler.postDelayed(sendUpdatesToUI, 1000); //01 sec
        //
    }

    private Runnable sendUpdatesToUI = new Runnable() {
        public void run() {
//            DisplayLoggingInfo();
            getXmlInfo();
            handler.postDelayed(this, 1000);
        }
    };

    /*private void DisplayLoggingInfo() {
        Log.d(TAG, "entered DisplayLoggingInfo");

        intent.putExtra("time", new Date().toLocaleString());
        intent.putExtra("counter", String.valueOf(++counter));
        sendBroadcast(intent);
    }
*/
    private void getXmlInfo() {
//        final ProgressDialog loading = ProgressDialog.show(
//                getActivity(), "Fetching datA", "plZ w8..", false, false);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(API_BASE_URL)
                .client(new OkHttpClient())
                .addConverterFactory(SimpleXmlConverterFactory.create())
                .build();

        ProductAPI api = retrofit.create(ProductAPI.class);

        Call<Rss> call = api.getProducts();
        call.enqueue(new Callback<Rss>() {
            @Override
            public void onResponse(Call<Rss> call, Response<Rss> response) {

                SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                SharedPreferences.Editor editor = sharedPrefs.edit();
                Gson gson = new Gson();

                String json = gson.toJson(response.body().getChannel().getItem());

                editor.putString("T", json);
                editor.commit();

            }

            @Override
            public void onFailure(Call<Rss> call, Throwable t) {
//                loading.dismiss();
                Log.e("javad",t.getMessage());
                System.out.println(t.getLocalizedMessage());
            }
        });

    }


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        handler.removeCallbacks(sendUpdatesToUI);
        super.onDestroy();
    }
}
