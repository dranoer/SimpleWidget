package com.androiddevtwo.isatel.mywidget.rss;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.ArrayList;

/**
 * Created by afkari on 11/7/17.
 */


@Root(name = "item")
public class RssItem {
    @Element(name = "title")
    public String title;
    @Element(name = "description")
    public String description;
    @Element(name = "link")
    public String link;
    @Element(name = "author")
    public String author;
    @Element(name = "category")
    public String category;
    @Element(name = "pubDate")
    public String pubDate;

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getLink() {
        return link;
    }

    public String getAuthor() {
        return author;
    }

    public String getCategory() {
        return category;
    }

    public String getPubDate() {
        return pubDate;
    }

}