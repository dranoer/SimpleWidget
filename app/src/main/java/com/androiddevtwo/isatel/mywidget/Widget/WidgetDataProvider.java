package com.androiddevtwo.isatel.mywidget.Widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.ListView;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

import com.androiddevtwo.isatel.mywidget.ProductAPI;
import com.androiddevtwo.isatel.mywidget.R;
import com.androiddevtwo.isatel.mywidget.rss.Rss;
import com.androiddevtwo.isatel.mywidget.rss.RssItem;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.SimpleXmlConverterFactory;

import static android.content.ContentValues.TAG;

/**
 * Created by AndroidDev2 on 11/12/2017.
 */

@SuppressLint("NewApi")
public class WidgetDataProvider implements RemoteViewsService.RemoteViewsFactory {

    List mCollections = new ArrayList();
    Context mContext = null;

/*    SharedPreferences sharedPrefs;
    Gson gson;
    String json;
    Type type;*/
//    ArrayList<RssItem> arrayList;

    public WidgetDataProvider(Context context, Intent intent) {
        mContext = context;
    }

/*    public WidgetDataProvider(SharedPreferences sharedPrefs, Gson gson, String json, Type type) {
        this.sharedPrefs = sharedPrefs;
        this.gson = gson;
        this.json = json;
        this.type = type;
//        this.arrayList = arrayList;
    }*/

    @Override
    public int getCount() {
        return mCollections.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public RemoteViews getLoadingView() {
        return null;
    }

    @Override
    public RemoteViews getViewAt(int position) {

        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(mContext);
        Gson gson = new Gson();
        String json = sharedPrefs.getString("T", null);
        Type type = new TypeToken<ArrayList<RssItem>>() {}.getType();
        ArrayList<RssItem> arrayList = gson.fromJson(json, type);

/*        sharedPrefs = PreferenceManager.getDefaultSharedPreferences(mContext);
        json = sharedPrefs.getString("T", null);
        type = new TypeToken<ArrayList<RssItem>>() {}.getType();
        ArrayList<RssItem> arrayList = gson.fromJson(json, type);*/

        RemoteViews mView = new RemoteViews(mContext.getPackageName(), android.R.layout.simple_list_item_1);
        mView.setTextViewText(android.R.id.text1, arrayList.get(position).getTitle());
//                (CharSequence) arrayList.get(position));

        mView.setTextColor(android.R.id.text1, Color.BLACK);

        return mView;

/*
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        String value = preferences.getString("Stitle", title);
//                .getString(R.string.pref_default);
//        String tt = preferences.getString("Stitle", value);

        RemoteViews mView = new RemoteViews(mContext.getPackageName(), android.R.layout.simple_list_item_1);
        mView.setTextViewText(android.R.id.text1,
                value);
//                (CharSequence) mCollections.get(position));
        mView.setTextColor(android.R.id.text1, Color.BLACK);

        return mView;*/
    }

    @Override
    public int getViewTypeCount() {
        return 4;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public void onCreate() {
        initData();
    }

    @Override
    public void onDataSetChanged() {
        initData();
    }

    private void initData() {
        mCollections.clear();
        for (int i = 1; i <= 4; i++) {
            mCollections.add("item " + i);
        }
    }

    @Override
    public void onDestroy() {

    }







}
