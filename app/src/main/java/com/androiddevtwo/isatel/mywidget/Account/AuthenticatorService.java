package com.androiddevtwo.isatel.mywidget.Account;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

/**
 * Created by AndroidDev2 on 11/8/2017.
 */

//bind the Authenticator to the Framework
public class AuthenticatorService extends Service {

    //instance field that stores the authenticator object
    private Authenticator mAuthenticator;

    @Override
    public void onCreate() {
        //creatE nEw authenticator obj
        mAuthenticator = new Authenticator(this);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mAuthenticator.getIBinder();
    }
}
