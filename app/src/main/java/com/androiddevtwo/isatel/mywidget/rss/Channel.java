package com.androiddevtwo.isatel.mywidget.rss;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.NamespaceList;
import org.simpleframework.xml.Root;

import java.util.ArrayList;


/**
 * Created by afkari on 11/7/17.
 */

@Root(name = "channel", strict = false)
@NamespaceList({
        @Namespace(reference = "http://www.w3.org/2001/XMLSchema-instance", prefix = "xsi"),
        @Namespace(reference = "http://www.w3.org/2001/XMLSchema", prefix = "xsd")
})
public class Channel {

    @ElementList(inline = true, required = true)
    public ArrayList<RssItem> item;

    public ArrayList<RssItem> getItem(){
        return item;
    }
}
