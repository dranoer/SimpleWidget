package com.androiddevtwo.isatel.mywidget.Account;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

/**
 * Created by AndroidDev2 on 11/11/2017.
 */

//this service returns an IBinder for the sync adapter class,
// allows the sync adapter framework to call onPerformSync()
public class SyncService extends Service {

    //storage for an instance of the sync adapter
    private static SyncAdapter sSyncAdapter = null;

    //object to use a thread-safe lock
    private static final Object sSyncAdapterLock = new Object();

    @Override
    public void onCreate() {
        synchronized (sSyncAdapterLock) {
            if (sSyncAdapter == null) {
                sSyncAdapter = new SyncAdapter(getApplicationContext(), true);
            }
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return sSyncAdapter.getSyncAdapterBinder();
    }
}
