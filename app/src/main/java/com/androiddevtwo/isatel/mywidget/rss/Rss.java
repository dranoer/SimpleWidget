package com.androiddevtwo.isatel.mywidget.rss;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by AndroidDev2 on 11/5/2017.
 */

@Root(name = "rss")
public class Rss {

    @Attribute
    private String version;

    @Element(required = true)
    private Channel channel;

    public Channel getChannel() {
        return channel;
    }
    private String getVersion() {
        return version;
    }

}
