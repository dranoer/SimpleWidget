package com.androiddevtwo.isatel.mywidget;

import android.accounts.Account;
import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

/**
 * Created by AndroidDev2 on 11/8/2017.
 */

public class MainActivity extends AppCompatActivity {

    public static final String AUTHORITY = "com.androiddevtwo.isatel.mywidget";
    public static final Account ACCOUNT = new Account("example","com.androiddevtwo.isatel.mywidget");

    private static final String TAG = "MainActivity";
    private Intent intent;

    // Sync interval constants
    public static final long SECONDS_PER_MINUTE = 60L;
    public static final long SYNC_INTERVAL_IN_MINUTES = 60L;
    public static final long SYNC_INTERVAL =
            SYNC_INTERVAL_IN_MINUTES *
                    SECONDS_PER_MINUTE;

    ContentResolver mResolver;
    String title;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //get the content resolver for your app
        mResolver = getContentResolver();

         /*
         * Turn on periodic syncing
         */
         getContentResolver().addPeriodicSync(
                 ACCOUNT, AUTHORITY, Bundle.EMPTY, SYNC_INTERVAL);

        setContentView(R.layout.activity_main);

        intent = new Intent(this, DataBroadcastService.class);
    }

    //receive the message that is going to be broadcast from the Service
    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            updateUI(intent);
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        startService(intent);
        registerReceiver(broadcastReceiver, new IntentFilter(DataBroadcastService.BROADCAST_ACTION));
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(broadcastReceiver);
        stopService(intent);
    }

    private void updateUI(Intent intent) {

//        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
//        String tt = preferences.getString("titlep", title);

/*        String tt = intent.getStringExtra("titlep");
//        Log.d(TAG, counter);
        TextView txtCounter = (TextView) findViewById(R.id.txtCounter);
        txtCounter.setText(tt);*/
    }

    @Override
    protected void onDestroy() {
        //stop the service, cause if i do not stop it by myself, the service will die with my app,
        //by stopping the service, i will force the service to call it's own onDestroy
        // which will force it to recreate itself after rge app is dead
        //stopService(mServiceIntent);
        Log.i("MAINACT", "onDestroy!");
        super.onDestroy();
    }

}
