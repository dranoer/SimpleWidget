package com.androiddevtwo.isatel.mywidget;

import com.androiddevtwo.isatel.mywidget.rss.Rss;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by AndroidDev2 on 11/5/2017.
 */

public interface ProductAPI {

    @GET("/ExRatesRss.aspx")
    Call<Rss> getProducts();

}
